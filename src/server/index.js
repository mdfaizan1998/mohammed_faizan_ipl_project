const fs = require("fs");
const csv = require("csvtojson");
const iplfunctions=require("./ipl");
const matchesPlayedPerYear=iplfunctions.matchesPlayedPerYear;
const matchesWonByEachTeam=iplfunctions.matchesWonByEachTeam;
const extraRunConcededByEachTeam2016=iplfunctions.extraRunConcededByEachTeam2016;
const topTenEconomicBowlers2015=iplfunctions.topTenEconomicBowlers2015;


const json_Output_1="./src/public/output/matchesPlayedPerYear.json";
const json_Output_2="./src/public/output/matchesWonByEachTeam.json";
const json_Output_3="./src/public/output/extraRunConcededByEachTeam2016.json";
const json_Output_4="./src/public/output/topTenEconomicBowlers2015.json";


const MATCHES_FILE_PATH = "./src/data/matches.csv";
const DELIVERIES_FILE_PATH = "./src/data/deliveries.csv";


let JSON_OUTPUT_FILE_PATH=[json_Output_1,
                           json_Output_2,
                           json_Output_3,
                           json_Output_4];


function main() {
  csv()
     .fromFile(MATCHES_FILE_PATH)
     .then(matches =>{

     let result1 = matchesPlayedPerYear(matches);
    //  console.log(result1);
    let result2 = matchesWonByEachTeam(matches);
    //console.log(result2);
 csv()
      .fromFile(DELIVERIES_FILE_PATH)
      .then(deliveries => {
          let result3 = extraRunConcededByEachTeam2016(matches,deliveries);
          //console.log(result3);
          let result4 = topTenEconomicBowlers2015(matches,deliveries);
          //console.log(result4);
            saveData([result1, result2, result3, result4], JSON_OUTPUT_FILE_PATH);
         
        });
  });
       

function saveData(data,JSON_OUTPUT_FILE_PATH) {
    let index=0;
    data.map(obj=>{
        let jsonData={};
        let values=Object.keys(obj);
    values.map(key=>{
      if(!jsonData[key]){
          jsonData[key]=obj[key];
      }
    });


    let finalJsonData=JSON.stringify(jsonData);
    fs.writeFile(JSON_OUTPUT_FILE_PATH[index++],finalJsonData, (err)=>{
      if(err){
      console.log(err);
      }
    });
    });

  }
}
  main();
